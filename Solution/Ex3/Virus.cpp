#include "Virus.h"

/* function sets initial values for virus */
void Virus::init(const std::string RNA_sequence) {
	this->_RNA_sequence = RNA_sequence;
}

/* function inserts the virus's RNA to the cell */
void Virus::infect_cell(Cell& cell) const {
	int i = 0;
	Nucleus newNucleus;
	std::string infectedDNA = "";
	std::string virusDNA = "";
	// turn virus rna to dna
	for (i = 0; i < this->_RNA_sequence.length(); i++)
	{
		if (this->_RNA_sequence[i] == 'U') {
			virusDNA += 'T';
		}
		else {
			virusDNA += this->_RNA_sequence[i];
		}
	}

	// insert dna
	infectedDNA = cell.get_nucleus().get_DNA_strand();
	infectedDNA.insert(infectedDNA.length() / 2, virusDNA);

	// update nucleus
	newNucleus.init(infectedDNA);
	cell.set_nucleus(newNucleus);
}

