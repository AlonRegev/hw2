#include "Mitochondrion.h"

/* function sets initial values for the mitochondrion */
void Mitochondrion::init() {
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

/* function checks if a protein can be a glocuse receptor and updates the mitochondrion */
void Mitochondrion::insert_glocuse_receptor(const Protein& protein) {
	const AminoAcid glocuseReceptor[] = GLOCUSE_RECEPTOR;
	int count = 0;
	bool isValid = true;
	AminoAcidNode* currentAminoAcid = protein.get_first();

	// check each amino acid in the protein
	while (currentAminoAcid != nullptr && isValid) {
		isValid = count < GLOCUSE_RECEPTOR_LEN && currentAminoAcid->get_data() == glocuseReceptor[count];

		count++;
		currentAminoAcid = currentAminoAcid->get_next();
	}

	if (isValid && count == GLOCUSE_RECEPTOR_LEN) {	// protein matches a glocuse receptor
		this->_has_glocuse_receptor = true;
	}
}

/* function sets glocuse level */
void Mitochondrion::set_glocuse(const unsigned int glocuse_units) {
	this->_glocuse_level = glocuse_units;
}

/* function checks if mitochondrion can produce atp */
bool Mitochondrion::produceATP() const {
	return this->_has_glocuse_receptor && this->_glocuse_level >= GLOCUSE_REQ;
}
