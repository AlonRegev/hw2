#include "Ribosome.h"

/* function creates a protein from amino acids based on an rna transcript from the nucleus */
Protein* Ribosome::create_protein(std::string& RNA_transcript) const {
	int i = 0;
	std::string codon = "";
	AminoAcid currentAminoAcid = DEFAULT_AMINO_ACID;
	Protein* builtProtein = new Protein;
	builtProtein->init();
	// go over all codons and add their amino acid to the protein if they are not unknown
	for (i = 0; i + CODON_LEN <= RNA_transcript.length() && currentAminoAcid != UNKNOWN; i += CODON_LEN)
	{
		codon = RNA_transcript.substr(i, CODON_LEN);
		currentAminoAcid = get_amino_acid(codon);
		builtProtein->add(currentAminoAcid);
	}

	if (currentAminoAcid == UNKNOWN)	// invalid rna
	{
		builtProtein->clear();
		delete builtProtein;
		builtProtein = nullptr;
	}

	return builtProtein;
}
