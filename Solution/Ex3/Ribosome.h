#pragma once

#include "Protein.h"

#define CODON_LEN 3
#define DEFAULT_AMINO_ACID ALANINE

class Ribosome
{
public:
	Protein* create_protein(std::string& RNA_transcript) const;
};

