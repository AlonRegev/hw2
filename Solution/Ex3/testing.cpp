#include "Cell.h"

int tempMain() {
	std::string testDNA = "GCTTTAGGGCATTTATTTTAA";
	Gene gene;
	gene.init(0, 20, false);
	
	Cell cell;
	cell.init(testDNA, gene);

	std::cout << cell.get_ATP();

	return 0;
}