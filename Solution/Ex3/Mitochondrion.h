#pragma once

#include "Protein.h"

#define GLOCUSE_REQ 50
#define GLOCUSE_RECEPTOR { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END }
#define GLOCUSE_RECEPTOR_LEN 7

class Mitochondrion
{
public:
	void init();
	void insert_glocuse_receptor(const Protein& protein);
	void set_glocuse(const unsigned int glocuse_units);
	bool produceATP() const;
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
};

