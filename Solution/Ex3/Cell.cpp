#include "Cell.h"

/* function sets initial values of a Cell object */
void Cell::init(std::string dna_sequence, const Gene glucose_receptor_gene) {
	// init properties
	this->_mitochondrion.init();
	this->_nucleus.init(dna_sequence);

	this->_glucose_receptor_gene = glucose_receptor_gene;

	this->_atp_units = 0;
}

/* function checks all the requirements to create atp, and updates it's _atp_units */
bool Cell::get_ATP() {
	bool canCreateATP = false;
	std::string rna_transcript = this->_nucleus.get_RNA_transcript(this->_glucose_receptor_gene);
	Protein* protein = this->_ribosome.create_protein(rna_transcript);
	// check if protein is valid
	if (protein == nullptr) {
		std::cerr << "Failed to create protein";
		_exit(1);
	}
	
	// set mitochondreia to create atp
	this->_mitochondrion.set_glocuse(GLOCUSE_REQ);
	this->_mitochondrion.insert_glocuse_receptor(*protein);

	if (this->_mitochondrion.produceATP()) {
		canCreateATP = true;
		this->_atp_units = FULL_ATP;
	}

	protein->clear();
	delete protein;

	return canCreateATP;
}

// getters and setters
Nucleus Cell::get_nucleus() const {
	return this->_nucleus;
}
void Cell::set_nucleus(const Nucleus newValue) {
	this->_nucleus = newValue;
}
