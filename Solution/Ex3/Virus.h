#pragma once

#include "Cell.h"

class Virus
{
public:
	void init(const std::string RNA_sequence);
	void infect_cell(Cell& cell) const;
private:
	std::string _RNA_sequence;
};

