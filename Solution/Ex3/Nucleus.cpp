#include "Nucleus.h"

/* function sets initial values of class */
void Gene::init(const unsigned int start, const unsigned int end, const unsigned int on_complementary_dna_strand) {
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// getters and setters
unsigned int Gene::get_start() const {
	return this->_start;
}
void Gene::set_start(const unsigned int newValue) {
	this->_start = newValue;
}
unsigned int Gene::get_end() const {
	return this->_end;
}
void Gene::set_end(const unsigned int newValue) {
	this->_end = newValue;
}
bool Gene::is_on_complementary_dna_strand() const {
	return this->_on_complementary_dna_strand;
}
void Gene::set_on_complementary_dna_strand(const bool newValue) {
	this->_on_complementary_dna_strand = newValue;
}

/* function sets Nucleus dna and creates it's complementary */
void Nucleus::init(const std::string dna_sequence) {
	unsigned int i = 0;
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";

	for (i = 0; i < dna_sequence.length(); i++) {
		switch (dna_sequence[i]) {
		case 'A':
			this->_complementary_DNA_strand += 'T';
			break;
		case 'T':
			this->_complementary_DNA_strand += 'A';
			break;
		case 'C':
			this->_complementary_DNA_strand += 'G';
			break;
		case 'G':
			this->_complementary_DNA_strand += 'C';
			break;
		default:
			// invalid nucleotide
			std::cerr << "DNA has invalid nucleotide (" << dna_sequence[i] << ")";
			_exit(1);
		}
	}
}

/* function creates an RNA transcript from the Nucleus DNA based on the Gene */
std::string Nucleus::get_RNA_transcript(const Gene& gene) const {
	std::string rna = "";
	unsigned int i = 0;
	const std::string& dna_strand = gene.is_on_complementary_dna_strand() ? this->_complementary_DNA_strand : this->_DNA_strand;
	// go over the gene range and create rna
	for (i = gene.get_start(); i <= gene.get_end(); i++) {
		if (dna_strand[i] == 'T')
			rna += 'U';
		else
			rna += dna_strand[i];
	}

	return rna;
}

/* function return a reversed dna strand (first one, not complementary) */
std::string Nucleus::get_reversed_DNA_strand() const {
	std::string reverse = "";
	unsigned int i = 0;
	for (i = this->_DNA_strand.length(); i > 0; i--) {
		reverse += this->_DNA_strand[i - 1];
	}
	return reverse;
}

/* function returns number of times a codon appears in the first dna strand */
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const {
	unsigned int i = 0;
	int count = 0;
	// check if there's a codon between i to i + 2
	for (i = 0; i < this->_DNA_strand.length() - CODON_LEN + 1; i++) {
		if (this->_DNA_strand.substr(i, CODON_LEN) == codon) {
			count++;
		}
	}

	return count;
}

// getters and setters
std::string Nucleus::get_DNA_strand() const {
	return this->_DNA_strand;
}
std::string Nucleus::get_complementary_DNA_strand() const {
	return this->_complementary_DNA_strand;
}
