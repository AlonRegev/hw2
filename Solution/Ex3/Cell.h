#pragma once

#include "Nucleus.h"
#include "Ribosome.h"
#include "Mitochondrion.h"

#define FULL_ATP 100

class Cell
{
public:
	void init(std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();

	// getters and setters
	Nucleus get_nucleus() const;
	void set_nucleus(const Nucleus newValue);
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glucose_receptor_gene;
	unsigned int _atp_units;
};

